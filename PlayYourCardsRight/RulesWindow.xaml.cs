﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PlayYourCardsRight
{
    /// <summary>
    /// Interaction logic for RulesWindow.xaml
    /// </summary>
    public partial class RulesWindow
    {
        public RulesWindow()
        {
            InitializeComponent();
            this.AllowsTransparency = true;
        }

        //Transition out of the rules window
        private void btn_Back_Click(object sender, RoutedEventArgs e)
        {
            Storyboard sb = this.FindResource("TransitionOut") as Storyboard;
            sb.Begin();
            MenuWindow mw = new MenuWindow();
            mw.Show();
        }

        private void OnAnimationClose(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
