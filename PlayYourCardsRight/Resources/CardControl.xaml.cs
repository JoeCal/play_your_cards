﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PlayYourCardsRight.Resources
{
    /// <summary>
    /// Interaction logic for CardControl.xaml
    /// </summary>
    public partial class CardControl
    {
        public CardControl()
        {
            InitializeComponent();
        }

        //For getting the card controls properties
        public CardGame.Card GetCardProperties()
        {
            CardGame.Card card = new CardGame.Card
            {
                Rank = Convert.ToInt32(RankLabel.Tag),
                Suit = Convert.ToString(SuitImage.Tag)
            };
            return card;
        }

        //Get the properties of a card and display them on the control
        public void SetCardProperties(CardGame.Card CardObject)
        {
            Uri imageSource;

            switch (CardObject.Rank)
            {
                case 10:
                    RankLabel.Content = "A";
                    break;
                case 11:
                    RankLabel.Content = "J";
                    break;
                case 12:
                    RankLabel.Content = "Q";
                    break;
                case 13:
                    RankLabel.Content = "K";
                    break;
                default:
                    RankLabel.Content = CardObject.Rank;
                    break;
            }
            RankLabel.Tag = CardObject.Rank;
            RankLabelUpsideDown.Content = RankLabel.Content;

            switch (CardObject.Suit)
            {
                case "Spades":
                    imageSource = new Uri(@"/PlayYourCardsRight;component/assets/spade.png", UriKind.Relative);
                    SuitImage.Source = new BitmapImage(imageSource);
                    break;
                case "Hearts":
                    imageSource = new Uri(@"/PlayYourCardsRight;component/assets/heart.png", UriKind.Relative);
                    SuitImage.Source = new BitmapImage(imageSource);
                    break;
                case "Diamonds":
                    imageSource = new Uri(@"/PlayYourCardsRight;component/assets/diamond.png", UriKind.Relative);
                    SuitImage.Source = new BitmapImage(imageSource);
                    break;
                case "Clubs":
                    imageSource = new Uri(@"/PlayYourCardsRight;component/assets/club.png", UriKind.Relative);
                    SuitImage.Source = new BitmapImage(imageSource);
                    break;
            }

            SuitImage.Tag = CardObject.Suit;

        }
    }
}
