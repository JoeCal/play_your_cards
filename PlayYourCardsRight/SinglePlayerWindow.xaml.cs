﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using PlayYourCardsRight.Resources;

namespace PlayYourCardsRight
{
    /// <summary>
    /// Interaction logic for SinglePlayerWindow.xaml
    /// </summary>
    public partial class SinglePlayerWindow
    {
        //For retrieving from the cache
        // Written to in MainWindow
        ObjectCache cache = MemoryCache.Default;

        private FileHandler fileHandler;
        private CardGame game;

        private Queue<CardGame.Card> deck;
        private Resources.CardControl[] cardControls;

        private int currentCard = 0;

        private CardGame.User user;




        public SinglePlayerWindow()
        {
            game = new CardGame();
            fileHandler = new FileHandler();

            fileHandler.SetFilePath(@"UserFile.json");

            this.AllowsTransparency = true;
            InitializeComponent();

            //Create an array of the CardControls used in the window
            // To be used for "Flipping" each card later
            cardControls = new PlayYourCardsRight.Resources.CardControl[] { Card1, Card2, Card3, Card4, Card5 }; 

            //Generate a deck of cards objects - contains the properties of the cards
            // Assign the card properties to the CardControl (Display it to the user)
            deck = game.GenerateDeck();
            Card1.SetCardProperties(deck.Dequeue());

            //Retrieve the user object from the cache and display the name property in the window
            user = (CardGame.User)cache.Get("UserObject");
            this.ChpNameChip.Content = user.Name;

            //Flip the first card over, with a delay of 100ms (animation compensation)
            FlipCard(Card1,100);
        }

        //For all button clicks while game not started
        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            if (sender.Equals(BtnGo))
            {
                StartGame(0);
            }

            if (sender.Equals(BtnSwitch))
            {
                this.BtnSwitch.IsHitTestVisible = false;
                SwitchCard(Card1, deck.Dequeue());

                StartGame(1000);
            }

            if (sender.Equals(BtnGo))
            {
                StartGame(0);
            }

            if (sender.Equals(BtnBackToMenu))
            {
                //Close the score dialog
                this.scoreDialog.IsOpen = false;
                //Transition the window out
                Storyboard sb = this.FindResource("TransitionOut") as Storyboard;
                sb.Begin();
                //Open the menu window
                MenuWindow menuWindow = new MenuWindow();
                menuWindow.Show();
            }
        }

        private void GameButtonsClicked(object sender, RoutedEventArgs e)
        {
            bool outcome;
            bool higher = false;
            DisableGameButtons();

            //Move to the next card control
            currentCard += 1;
            //Flip it over
            cardControls[currentCard].Flipper.IsFlipped = true;

            //Check if the user is correct
            if (sender.Equals(btnHigher))
            {
                higher = true;
            }
            outcome = game.CheckIfCorrect(higher, cardControls[currentCard - 1].GetCardProperties(), cardControls[currentCard].GetCardProperties());

            //Display the outcome to the user (Storyboard animation)
            if (outcome)
            {
                Storyboard sb = this.FindResource("AnswerCorrect") as Storyboard;
                sb.Begin();
            }
            else
            {
                Storyboard sb = this.FindResource("AnswerIncorrect") as Storyboard;
                sb.Begin();
            }
            this.ChpCircle.Content = game.GetPoints();

            //After, check if the game has ended (all cards flipped)
            CheckGameEnd();
        }

        private async void StartGame(int delay)
        {
            //Use a delay to compensate for animation times
            // Not entirely elegant
            await Task.Delay(delay);

            //Begin the startgame animation
            Storyboard sb = this.FindResource("StartGame") as Storyboard;
            sb.Begin();

            //Assign properties to the card controls from cards in the deck
            for (int i = 1; i < cardControls.Length; i++)
            {
                cardControls[i].SetCardProperties(deck.Dequeue());
            }
        }
   
        //Check if the game is ended and display a score dialog if it is
        private void CheckGameEnd()
        {
            if(currentCard == 4)
            {
                this.scoreDialog.IsOpen = true;
                //Update the label in the dialog to show the score
                this.lblPoints.Content = game.GetPoints();
                user.Score = game.GetPoints();

                //Add the current user object to the scores file
                fileHandler.AppendToFile(user);
            }
            else
            {
                EnableGameButtons();
            }

        }

        //Disable game buttons during animation to prevent clicking
        private void DisableGameButtons()
        {
            this.btnLower.IsHitTestVisible = false;
            this.btnHigher.IsHitTestVisible = false;
        }

        //Reenable buttons after animation
        private void EnableGameButtons()
        {
            this.btnLower.IsHitTestVisible = true;
            this.btnHigher.IsHitTestVisible = true;
        }

        //For when the switch card button is clicked
        private void SwitchCard(CardControl cardToFlip, CardGame.Card newCard)
        {
            //Flip the card with a delay
            //Ensures that card doesn't change properties until animation complete
            FlipCard(cardToFlip,500);
            cardToFlip.SetCardProperties(newCard);
            FlipCard(cardToFlip, 0);
        }

        private async void FlipCard(CardControl cardToFlip, int delay)
        {
            //Allow for a delay to compensate for animation times
            await Task.Delay(delay);
            cardToFlip.Flipper.IsFlipped = !cardToFlip.Flipper.IsFlipped;
        }


        //For closing the window once the transition out has completed
        private void OnAnimationClose(object sender, EventArgs e)
        {
            Close();
        }

    }
}
