﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Documents;
using Newtonsoft.Json;

namespace PlayYourCardsRight
{
    //For managing JSON files containing lists of User Objects (CardGame.User)
    class FileHandler
    {
        private String filePath;

        public void AppendToFile(CardGame.User userToWrite)
        {
            String jsonData;
            List<CardGame.User> userList = new List<CardGame.User>();

            //Check file exists, create if it doesn't
            if (File.Exists(filePath))
            {
                jsonData = System.IO.File.ReadAllText(filePath);
                userList = JsonConvert.DeserializeObject<List<CardGame.User>>(jsonData)
                           ?? new List<CardGame.User>();
            }

            userList.Add(userToWrite);

            //Using the JSON library, create a json string from the users list
            jsonData = JsonConvert.SerializeObject(userList, Formatting.Indented);

            //Write in a new thread so it isn't on UI thread
            Thread thread = new Thread(() => System.IO.File.WriteAllText(filePath, jsonData));
            thread.Start();        
        }

        public List<CardGame.User> ReadFile()
        {
            try
            {
                String json = File.ReadAllText(filePath);

                List<CardGame.User> users = JsonConvert.DeserializeObject<List<CardGame.User>>(json);
                return users;
            }
            catch (FileNotFoundException)
            {
                return new List<CardGame.User>();
            }


        }

        public void SetFilePath(String newPath)
        {
            filePath = newPath;
        }
    }
}
