﻿namespace PlayYourCardsRight
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using PlayYourCardsRight.Resources;

    public class CardGame
    {
        private int points;
        private int deck;


        //Compare initialCard and comparedCard, according to whether a user selected higher or lower
        public bool CheckIfCorrect(bool higher, Card initalCard, Card comparedCard)
        {
            if (higher & (initalCard.Rank < comparedCard.Rank))
            {
                points += 1;
                return true;
            }

            if (!higher & (initalCard.Rank > comparedCard.Rank))
            {
                points += 1;
                return true;
            }

            if (initalCard.Rank == comparedCard.Rank) return true;
            return false;
        }

        public Queue<Card> GenerateDeck()
        {
            var rndGen = new Random();
            Queue<Card> shuffledDeck;

            Card[] standardDeck;

            // Create unshuffled deck of cards
            standardDeck =

                // For every suit create cards with a rank  of 1-13
                new[] { "Spades", "Hearts", "Clubs", "Diamonds" }.SelectMany(
                    suit => Enumerable.Range(2, 12),

                    // Create an array of card objects
                    (suit, rank) => new Card { Rank = rank, Suit = suit }).ToArray();

            // Shuffle using the Fisher-Yates shuffling algorithm
            // In a nutshell, exchanges the card at position n and a card at a random index for the whole deck
            for (var n = standardDeck.Length - 1; n > 0; --n)
            {
                var randomIndex = rndGen.Next(n + 1);
                var temp = standardDeck[n];
                standardDeck[n] = standardDeck[randomIndex];
                standardDeck[randomIndex] = temp;
            }

            // Return as queue to utilize dequeue when selecting cards
            shuffledDeck = new Queue<Card>(standardDeck);
            return shuffledDeck;
        }

        public int GetPoints()
        {
            return points;
        }



        public class Card
        {
            public int Rank { get; set; }

            public string Suit { get; set; }
        }

        public class User
        {
            public string Name { get; set; }

            public int Score { get; set; }
        }
    }
}