﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.Caching;
using System.IO;

namespace PlayYourCardsRight
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        readonly ObjectCache cache = MemoryCache.Default;

        public MainWindow()
        {
            InitializeComponent();

            //Close the "No Name Entered" flyout
            this.FlyoutNoName.IsOpen = false;

            this.TbxNameEntry.GotFocus += CloseFlyoutOnFocus;
        }


        //Remove text from textbox on click
        private void DeleteTextboxText(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Text = string.Empty;
            //Remove handler to prevent accidental deletion
            tb.GotFocus -= DeleteTextboxText;
        }


        //For closing the flyout when a control is focused on
        private void CloseFlyoutOnFocus(object sender, RoutedEventArgs e) => this.FlyoutNoName.IsOpen = false;

        private void btn_NextButton_Click(object sender, RoutedEventArgs e)
        {


            //Check if name is entered
            if(String.IsNullOrWhiteSpace(this.TbxNameEntry.Text) || this.TbxNameEntry.Text == (string)Application.Current.FindResource("NameEntryHint"))
            {
                this.FlyoutNoName.IsOpen = true;
            }
            //If a name is entered, begin the transition between windows
            // Also, create a user object to be used later in the game
            else
            {
                CardGame.User user = new CardGame.User()
                {
                    Name = this.TbxNameEntry.Text,
                    Score = 0
                };

                //Add the current user object to the cache with no
                cache.Add("UserObject", user, null);

                //
                Storyboard sb = this.FindResource("WindowTransitionOut") as Storyboard;
                sb.Begin();

                MenuWindow menuWindow = new MenuWindow();
                menuWindow.Show();

                this.Close();
            }
        }


        //Close the window when transition out is finished
        private void OnAnimationClose(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
