﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;
using System.Windows.Media.Animation;

namespace PlayYourCardsRight
{
    /// <summary>
    /// Interaction logic for StatisticsWindow.xaml
    /// </summary>
    public partial class StatisticsWindow
    {
        private FileHandler fileHandler;
        public StatisticsWindow()
        {
            InitializeComponent();

            
            fileHandler = new FileHandler();
            fileHandler.SetFilePath(@"UserFile.json");

            //Create a list of user objects from the json file
            List<CardGame.User> userData = fileHandler.ReadFile();
            //Order it in descending in relation to the score
            userData = userData.OrderByDescending(o => o.Score).ToList();

            
            grid_UserScoreGrid.ItemsSource = userData;
        }



        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            //Transition back to the menu
            if(sender.Equals(btn_Back))
            {
                Storyboard sb = FindResource("TransitionOut") as Storyboard;
                sb.Begin();

                MenuWindow mw = new MenuWindow();
                mw.Show();
            }
        }
        
        

        private void OnAnimationClose(object sender, EventArgs e)
        {
            Close();
        }

        //Make sure the grid sorts by descending 
        private void grid_UserScoreGrid_Initialized(object sender, EventArgs e)
        {
            grid_UserScoreGrid.Columns.Last().SortDirection = System.ComponentModel.ListSortDirection.Descending;
        }
    }
}
