﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PlayYourCardsRight
{
    /// <summary>
    /// Interaction logic for MenuWindow.xaml
    /// </summary>
    public partial class MenuWindow
    {
        public MenuWindow()
        {
            InitializeComponent();
        }

        //When user selects an option begin the transition to those windows
        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            if (sender.Equals(BtnSinglePlayer))
            {
                Storyboard sb = FindResource("TransitionOut") as Storyboard;
                sb.Begin();

                SinglePlayerWindow sw = new SinglePlayerWindow();
                sw.Show();
            }

            if (sender.Equals(BtnStats))
            {
                Storyboard sb = FindResource("TransitionOut") as Storyboard;
                sb.Begin();

                StatisticsWindow stw = new StatisticsWindow();
                stw.Show();
            }

            if (sender.Equals(BtnHelp))
            {
                Storyboard sb = FindResource("TransitionOut") as Storyboard;
                sb.Begin();
                RulesWindow rw = new RulesWindow();
                rw.Show();
            }
        }

        //Close the window when transition out completes
        private void OnAnimationClose(object sender, EventArgs e)
        {
            Close();
        }

    }
}
